import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import {Link} from "react-router-dom";
const Sidebar=()=>{
    return( <nav className="col-md-2 d-none d-md-block bg-light sidebar">
    <div className="sidebar-sticky">
      <ul className="nav flex-column">
        <li className="nav-item">
          <Link className="nav-link active" to="/">
            Dashboard 
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/User">
            Users
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/Category">
            Category
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/Product">
            Products
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="#">
            Reports
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="#">
            Sale
          </Link>
        </li>
      </ul>
    </div>
  </nav>);
}
export default Sidebar;