import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";

const EditCategory = () => {
  let History = useHistory();
  const {id}=useParams();
  
  const [user, setCategory] = useState({
    main: "",
    sub: "",
    name: "",
    detail: "",
  });

  const { main, sub, name, detail } = user;

  const onInputChange = (e) => {
    setCategory({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(()=>{
      loadCategory();
  },[]);

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:3003/users/${id}`, user);
    History.push("/category");
  };

  //fuction for update category details
  const loadCategory = async () => {
    const result = await axios.get(`http://localhost:3003/users/${id}`);
    setCategory(result.data);
  };

  return (
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
      <div className="w-75 mx-auto shadow p-5">
        <h1 className="text-center mb-4">Update Category details</h1>
        <form onSubmit={(e) => onSubmit(e)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter main category type"
              name="main"
              value={main}
              onChange={(e) => onInputChange(e)}
            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Sub category"
              name="sub"
              value={sub}
              onChange={(e) => onInputChange(e)}
            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter product name"
              name="name"
              value={name}
              onChange={(e) => onInputChange(e)}
            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter product description"
              name="detail"
              value={detail}
              onChange={(e) => onInputChange(e)}
            ></input>
          </div>
          <button className="btn btn-danger btn-block">Update</button>
        </form>
      </div>
    </main>
  );
};

export default EditCategory;
