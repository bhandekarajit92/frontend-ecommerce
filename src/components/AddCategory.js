import React, { useState } from "react";
import {useHistory} from 'react-router-dom'
import axios from "axios"
const AddCategory = () => {
    let History=useHistory();
  const [user, setCategory] = useState({
    main: "",
    sub: "",
    name: "",
    detail: "",
  });

const{main,sub,name,detail}=user;

const onInputChange=e=>{
setCategory({...user,[e.target.name]:e.target.value})
}


const onSubmit=async e=>{
    e.preventDefault(); 
    await axios.post("http://localhost:3003/users",user);
    History.push("/category")
}
  return (
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
      <div className="w-75 mx-auto shadow p-5">
        <h1 className="text-center mb-4">Add Category</h1>
        <form onSubmit={e=>onSubmit(e)}> 
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter main category type"
              name="main"
              value={main}
              onChange={(e) => onInputChange(e)}

            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Sub category"
              name="sub"
              value={sub}
              onChange={(e) => onInputChange(e)}

            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter product name"
              name="name"
              value={name}
              onChange={(e) => onInputChange(e)}

            ></input>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter product description"
              name="detail"
              value={detail}
              onChange={(e) => onInputChange(e)}
            ></input>
          </div>
          <button className="btn btn-primary btn-block">Add</button>
        </form>
      </div>
    </main>
  );
};

export default AddCategory;
