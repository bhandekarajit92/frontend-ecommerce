import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

const ViewCategory = () => {
  const [user, setCategory] = useState({
    main: "",
    sub: "",
    name: "",
    detail: "",
  });

  const { id } = useParams();

  useEffect(() => {
    loadCategory();
  }, []);

  const loadCategory = async () => {
    const result = await axios.get(`http://localhost:3003/users/${id}`);
    setCategory(result.data);
  };

  return (
    <>
      <div className="py-4">
        <Link className="btn btn-primary" to="/Category">
          Back to main menu
        </Link>
        <h1 className="display-4">Category Id:{id}</h1>
      </div>
      <hr />
      <ul className="list-group w-50">
        <li className="list-group-item">Main Category: {user.main} </li>
        <li className="list-group-item">Sub Category: {user.sub} </li>
        <li className="list-group-item">Product Name: {user.name} </li>
        <li className="list-group-item">Discription: {user.detail} </li>
      </ul>
    </>
  );
};
export default ViewCategory;
