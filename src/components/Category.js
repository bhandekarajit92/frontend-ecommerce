import React, { useState, useEffect } from "react";
import axios from "axios";
import {Link} from 'react-router-dom'
const Category = () => {
  const [users, setUser] = useState([]);

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get("http://localhost:3003/users");
    setUser(result.data.reverse());
  };

const deleteCategory=async id=>{
await axios.delete(`http://localhost:3003/users/${id}`);
loadUsers();
}




  return (
    <>
     <div className="py-4">
         <Link className="btn btn-outline-primary" to="/AddCategory">Add Category</Link>
     </div>
     <div className="py-4">
      <table className="table border shadow">
        <thead className="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Main category</th>
            <th scope="col">Sub Category</th>
            <th scope="col">Title</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((data, index) => (
            <tr>
              <th scope="row">{index + 1}</th>
              <td>{data.main}</td>
              <td>{data.sub}</td>
              <td>{data.name}</td>
              <td>
                  <Link className="btn btn-primary mr-2" to={`/${data.id}`}>View</Link>
                  <Link className="btn btn-outline-primary mr-2" to={`/EditCategory/${data.id}`}>Update</Link>
                  <Link className="btn btn-danger" onClick={()=>deleteCategory(data.id)}>Delete</Link>

              </td>
            </tr>
          ))}
        </tbody>
      </table>
</div>
</>
  );
};

export default Category;
