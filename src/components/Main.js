import React from "react";

import Dashboard from "./Dashboard";

import Category from "./Category";
import Product from "./Product";
import User from "./User";
import AddCategory from "./AddCategory";
import EditCategory from "./EditCategory";
import ViewCategory from "./ViewCategory";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const Main = () => {
  return (
    <Switch>
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/user" component={User} />
      <Route exact path="/category" component={Category} />
      <Route exact path="/product" component={Product} />
      <Route exact path="/AddCategory" component={AddCategory} />
      <Route exact path="/EditCategory/:id" component={EditCategory} />
      <Route exact path="/:id" component={ViewCategory} />
    </Switch>
  );
};
export default Main;
