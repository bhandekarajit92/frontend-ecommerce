import React from "react";
import Main from "./Main";
import Sidebar from "./Sidebar";
import Navbar from "./Navbar";

const Home = () => {
  return (
    <>
    
      <div class="container-fluid">
        <div class="row">
          <Sidebar />
          <Main />
        </div>
      </div>
    </>
  );
};
export default Home;
